package de.ixilon.osm.api;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import de.ixilon.jaxb.JaxbConverter;
import de.ixilon.osm.schema.ObjectFactory;
import de.ixilon.osm.schema.OsmRoot;

public class OverpassClient {

  private final RestTemplate restTemplate;
  private final URI url;

  /**
   * Create HTTP client.
   */
  public OverpassClient(URI url) {
    this(url, new RestTemplate());
  }

  /**
   * Create HTTP client.
   */
  public OverpassClient(URI url, RestTemplate restTemplate) {
    JaxbConverter<OsmRoot> converter =
        new JaxbConverter<OsmRoot>(OsmRoot.class, ObjectFactory.class);
    this.url = url;
    this.restTemplate = restTemplate;
    this.restTemplate.getMessageConverters().add(converter);
  }

  /**
   * Fetch Overpass XML query.
   */
  public OsmRoot get(String query) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUri(url);
    builder.query(query);
    return restTemplate.getForEntity(builder.build().encode().toUri(), OsmRoot.class).getBody();
  }

  /**
   * Fetch Overpass XAPI query.
   */
  public OsmRoot post(String query) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_XML);
    HttpEntity<String> request = new HttpEntity<>(query, headers);
    return restTemplate.exchange(url, HttpMethod.POST, request, OsmRoot.class).getBody();
  }

}

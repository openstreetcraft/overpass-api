package de.ixilon.jaxb;

import java.io.IOException;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

public class JaxbConverter<T> extends AbstractHttpMessageConverter<T> {

  private final Class<T> clazz;
  private final Unmarshaller unmarshaller;
  private final Marshaller marshaller;

  /**
   * Constructs an HTTP message converter with supported media types
   * text/xml and application/xml.
   */
  public JaxbConverter(Class<T> clazz, Class<?> factory) {
    super(MediaType.TEXT_XML, MediaType.APPLICATION_XML, new MediaType("application", "*+xml"));
    this.clazz = clazz;
    try {
      JAXBContext context = JAXBContextFactory.createContext(new Class[] {factory}, null);
      unmarshaller = context.createUnmarshaller();
      marshaller = context.createMarshaller();
    } catch (JAXBException exception) {
      throw new IllegalArgumentException(clazz.getName(), exception);
    }
  }

  @Override
  protected boolean supports(Class<?> clazz) {
    return clazz.isAssignableFrom(this.clazz);
  }

  @Override
  protected T readInternal(Class<? extends T> clazz, HttpInputMessage inputMessage)
      throws IOException, HttpMessageNotReadableException {
    try {
      @SuppressWarnings("unchecked")
      JAXBElement<T> response = (JAXBElement<T>) unmarshaller.unmarshal(inputMessage.getBody());
      return response.getValue();
    } catch (JAXBException exception) {
      throw new HttpMessageNotReadableException("unmarshall error", exception);
    }
  }

  @Override
  protected void writeInternal(T object, HttpOutputMessage outputMessage)
      throws IOException, HttpMessageNotWritableException {
    try {
      marshaller.marshal(object, outputMessage.getBody());
    } catch (JAXBException exception) {
      throw new HttpMessageNotWritableException("marshall error", exception);
    }
  }

}

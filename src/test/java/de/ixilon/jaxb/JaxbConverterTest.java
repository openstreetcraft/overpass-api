package de.ixilon.jaxb;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;

import de.ixilon.osm.schema.ObjectFactory;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmRoot;

public class JaxbConverterTest {

  private JaxbConverter<OsmRoot> converter;
  private HttpInputMessage inputMessage;

  @Before
  public void setUp() {
    converter = new JaxbConverter<>(OsmRoot.class, ObjectFactory.class);
    inputMessage = new HttpInputMessage() {

      @Override
      public HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application/osm3s+xml");
        headers.setContentType(mediaType);
        return headers;
      }

      @Override
      public InputStream getBody() throws IOException {
        return JaxbConverterTest.class.getResourceAsStream("response.xml");
      }
    };
  }

  @Test
  public void decodesGeneratorFromResponse() throws IOException {
    String expected = "Overpass API";
    String actual = converter.readInternal(OsmRoot.class, inputMessage).getGenerator();
    assertEquals(expected, actual);
  }

  @Test
  public void decodesVersionFromResponse() throws IOException {
    BigDecimal expected = new BigDecimal("0.6");
    BigDecimal actual = converter.readInternal(OsmRoot.class, inputMessage).getVersion();
    assertEquals(expected, actual);
  }

  @Test
  public void decodesFirstNodeFromResponse() throws IOException {
    for (Object obj : converter.readInternal(OsmRoot.class, inputMessage)
        .getNodeOrRelationOrWay()) {
      if (obj instanceof OsmNode) {
        OsmNode node = (OsmNode) obj;

        assertEquals(533377, node.getId());
        assertEquals(49.8650015, node.getLat(), Double.MIN_VALUE);
        assertEquals(8.6033513, node.getLon(), Double.MIN_VALUE);

        return;
      }
    }
    throw new AssertionError("OsmNode not found");
  }

}

package de.ixilon.osm.api;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Test;

public class OverpassClientTest {

  @Test
  public void decodesXapiResponseForBoundingBox() throws URISyntaxException {
    OverpassClient client = new OverpassClient(new URI("http://overpass-api.de/api/xapi"));
    String query = "*[bbox=48,11,48.00001,11.00001]";
    String expected = "Overpass API";
    String actual = client.get(query).getGenerator();
    assertThat(actual, containsString(expected));
  }

  @Test
  public void decodesOverpassXmlResponseForNodeId() throws URISyntaxException {
    OverpassClient client = new OverpassClient(new URI("http://overpass-api.de/api/interpreter"));
    String query = "<id-query type='node' ref='4571362977'/>"
        + "<print/>";
    int actual = client.post(query).getNodeOrRelationOrWay().size();
    assertThat(actual, equalTo(1));
  }

  @Test
  public void decodesOverpassXmlResponseForWayId() throws URISyntaxException {
    OverpassClient client = new OverpassClient(new URI("http://overpass-api.de/api/interpreter"));
    String query = "<union>" 
        + "<id-query type='way' ref='376941617'/>"
        + "<recurse type='way-node'/>"
        + "</union>"
        + "<print/>";
    int actual = client.post(query).getNodeOrRelationOrWay().size();
    assertThat(actual, greaterThan(1));
  }

  @Test
  public void decodesOverpassXmlResponseForBoundingBox() throws URISyntaxException {
    OverpassClient client = new OverpassClient(new URI("http://overpass-api.de/api/interpreter"));
    String query = "<union>"
        + "<bbox-query s='51.2500' w='7.1480' n='51.2501' e='7.1481'/>"
        + "<recurse type='up'/>"
        + "</union>"
        + "<print mode='meta'/>";
    String expected = "Overpass API";
    String actual = client.post(query).getGenerator();
    assertThat(actual, containsString(expected));
  }

}

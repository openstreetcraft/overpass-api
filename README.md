# Overpass API Java client library #

[![build status](https://gitlab.com/openstreetcraft/overpass-api/badges/master/pipeline.svg)](https://gitlab.com/openstreetcraft/overpass-api/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.ixilon/overpass-api/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.ixilon/overpass-api)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/0d2faf9b062347578e82ad5c1364ef65)](https://www.codacy.com/gl/openstreetcraft/overpass-api/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=openstreetcraft/overpass-api&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/0d2faf9b062347578e82ad5c1364ef65)](https://openstreetcraft.gitlab.io/overpass-api/reports/jacoco/test/html/)
[![Javadoc Badge](https://img.shields.io/badge/javadoc-brightgreen.svg)](https://openstreetcraft.gitlab.io/overpass-api/docs/javadoc/)

[Overpass API](http://wiki.openstreetmap.org/wiki/Overpass_API) uses a HTTP interface to query an [OpenStreetMap](http://wiki.openstreetmap.org) server.

## Usage ##

### Overpass XML query ###

Use `OverpassClient.post()`:

```
OverpassClient client = new OverpassClient(new URI("http://overpass-api.de/api/interpreter"));
String query = "<union>" +
  "<bbox-query s='51.2500' w='7.1480' n='51.2501' e='7.1481'/>" +
  "<recurse type='up'/>" +
  "</union>" +
  "<print mode='meta'/>";
for (Object obj : client.post(query).getNodeOrRelationOrWay()) {
  if (obj instanceof OsmNode) {
    // ...
  }
}
```

### Overpass XAPI query ###

Use `OverpassClient.get()`:

```
OverpassClient client = new OverpassClient(new URI("http://overpass-api.de/api/xapi"));
String query = "node[bbox=48,11,48.00001,11.00001]";
for (Object obj : client.get(query).getNodeOrRelationOrWay()) {
  if (obj instanceof OsmNode) {
    // ...
  }
}
```

## Links ##
* [Overpass API](http://wiki.openstreetmap.org/wiki/Overpass_API)
